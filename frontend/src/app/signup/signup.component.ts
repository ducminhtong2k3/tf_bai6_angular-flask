import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgSelectOption, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  userForm: FormGroup

  constructor(
    private usersService: UsersService,
    private fb: FormBuilder,
    private route: Router
  ) {

    this.userForm = this.fb.group({

      name: ['', Validators.required],
      password: ['', Validators.required],

    })

   }

  ngOnInit(): void {
  }

  signup(forms: any): void {
    this.usersService.signup_service(this.userForm.value)
    .pipe(first()).subscribe(() => {
      this.route.navigate([''])
    })
  }
}
