import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { catchError, Observable } from 'rxjs';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(
    private inject: Injector,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authservice = this.inject.get(UsersService);
    let authreq = req;
    authreq = this.AddTokenheader(req, authservice.getToken());
    return next.handle(authreq).pipe(
      catchError(err => {
        if(err.status===401) {
          authservice.logout()
        }
        throw (err);
      })
    );
  }

  AddTokenheader(req: HttpRequest<any>, token: any) {
    return req.clone({
      headers: req.headers.set('Authorization', token)
    });
  }

}
