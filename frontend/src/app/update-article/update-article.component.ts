import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticlesService } from '../articles.service';

@Component({
  selector: 'app-update-article',
  templateUrl: './update-article.component.html',
  styleUrls: ['./update-article.component.css']
})
export class UpdateArticleComponent implements OnInit {

  angForm: FormGroup
  article: any
  id: any
  sub: any

  constructor(
    private articleService: ArticlesService,
    private fb: FormBuilder,
    private route: Router,
    private activeRoute: ActivatedRoute
  ) {
    
    this.angForm = this.fb.group({

      id: [''],
      title: ['', Validators.required],
      context: ['', Validators.required],

    });

    this.sub = this.activeRoute.paramMap
    .subscribe(params => {
      this.id = params.get('id')
    })

   }

  ngOnInit(): void {

    this.getSingleArticle(this.id)

  }

  getSingleArticle(id: any): void {
    this.articleService.getArticleByID(id)
    .subscribe((res: any) => {
      this.article = res;
      this.angForm.patchValue(res)
    })
  }

  updateArticle(arti: any) {
    this.articleService.updateArticle_service(arti.value)
    .subscribe((res: any) => {
      this.article = res;
      this.route.navigate([''])
    })
  }

}
