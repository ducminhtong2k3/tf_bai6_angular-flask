import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ListArticleComponent } from './list-article/list-article.component';
import { CreateArticleComponent } from './create-article/create-article.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { UpdateArticleComponent } from './update-article/update-article.component';
import { SingleArticleComponent } from './single-article/single-article.component';

const routes: Routes = [
  { path: '', component:ListArticleComponent },
  { path: 'create', component:CreateArticleComponent },
  { path: 'signup', component:SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'update/:id', component: UpdateArticleComponent },
  { path: ':id', component: SingleArticleComponent },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
