import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticlesService } from '../articles.service';

@Component({
  selector: 'app-single-article',
  templateUrl: './single-article.component.html',
  styleUrls: ['./single-article.component.css']
})
export class SingleArticleComponent implements OnInit {

  article: any
  sub: any
  id: any

  constructor(
    private articleService: ArticlesService,
    private activeRoute: ActivatedRoute,
  ) {

    this.sub = this.activeRoute.paramMap
    .subscribe(params => {
      this.id = params.get('id')
    })

   }

  ngOnInit(): void {

    this.singleArticle(this.id);

  }

  singleArticle(id: any) {
    this.articleService.getsingleArticle(id)
    .subscribe((data: any) => {
      this.article = data;
    })
  }

}
