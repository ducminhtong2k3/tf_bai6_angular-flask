import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { Users } from './users.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  baseUrl: string = "http://127.0.0.1:5000/"

  constructor(
    private httpClient: HttpClient,
    private route: Router,
  ) { }

  signup_service(userinfo: any) {
    return this.httpClient.post<any>(this.baseUrl + 'signup', userinfo)
    .pipe(map((Users: any) => {
      return Users;
    }))
  }

  proceedLogin(usercred: any) {
    return this.httpClient.post(this.baseUrl + 'login', usercred)
  }

  isLoggedIn() {
    return localStorage.getItem('token') != null;
  }

  getToken() {
    return localStorage.getItem('token') || '';
  }
  
  logout() {
    alert('Your session expired')
    localStorage.clear();
    this.route.navigate(['/login'])
  }
}
