import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs';
import { ArticlesService } from '../articles.service';

@Component({
  selector: 'app-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.css']
})
export class CreateArticleComponent implements OnInit {

  angForm: FormGroup

  constructor(
    private articlesService: ArticlesService,
    private fb: FormBuilder,
    private route: Router,
  ) {

    this.angForm = this.fb.group({

      title: ['', Validators.required],
      context: ['', Validators.required],

    })

   }

  ngOnInit(): void {
  }

  createArticle(forms: any): void {
    this.articlesService.createArticle_service(this.angForm.value)
    .pipe(first()).subscribe(() => {
      this.route.navigate(['']);
    });
  }

}
