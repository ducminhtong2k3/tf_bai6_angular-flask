import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../articles.service';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.css']
})
export class ListArticleComponent implements OnInit {

  articles: any
  isLoggedIn: any
  userLoggedIn: any

  constructor(
    private articlesService: ArticlesService,
    private userService: UsersService,
  ) { }

  ngOnInit(): void {

    this.articlesService.listArticle()
    .subscribe((data: any) => {
      this.articles = data.Articles;
    });

    if(localStorage.getItem('token') != null) {
      this.isLoggedIn = true
    }
    else {
      this.isLoggedIn = false
    }

    if (this.isLoggedIn == true) {
      this.userLoggedIn = localStorage.getItem('username')
    }
  }

  listArticle_byid(page_id: number) {
    this.articlesService.pageArticle(page_id)
    .subscribe((data: any) => {
      this.articles = data.Articles;
    })
  }

  deleteArticle(article_id: any): void {
    this.articlesService.deleteArticle_service(article_id)
    .subscribe(data => {
      {
        this.articlesService.listArticle()
        .subscribe((data: any) => {
          this.articles = data.Articles;
        });
      };
    });
  }

  logout() {
    this.userService.logout();
  }

}
