import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpInterceptor, HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http'
import { Articles } from './articles.model';
import { map, Observable } from 'rxjs';
import { UsersService } from './users.service';


@Injectable({
  providedIn: 'root'
})

export class ArticlesService {

  baseUrl: string = "http://127.0.0.1:5000/"

  constructor(
    private httpClient: HttpClient
  ) { }

  public listArticle() {
    return this.httpClient.get<Articles[]>(this.baseUrl);
  }

  public pageArticle(page_id: any) {
    return this.httpClient.get<Articles[]>(this.baseUrl + 'p' + page_id)
  }

  public createArticle_service(articleinfo: any) {
    return this.httpClient.post<any>(this.baseUrl + 'create', articleinfo)
      .pipe(map((Articles: any) => {
        return Articles;
      }))
  }

  deleteArticle_service(id: any) {
    const params = new HttpParams().set('id', id)
    return this.httpClient.get(this.baseUrl + 'delete/' + id)
  }

  getArticleByID(id: number) {
    return this.httpClient.get<Articles>(this.baseUrl + id)
  }

  getsingleArticle(id: number) {
    return this.httpClient.get<Articles[]>(this.baseUrl + id)
  }

  updateArticle_service(articles: any) {
    return this.httpClient.put<Articles>(this.baseUrl + 'update/' + articles.id, articles)
  }

}
