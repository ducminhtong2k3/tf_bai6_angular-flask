import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm: FormGroup
  token: any
  responsedata: any

  constructor(
    private userService: UsersService,
    private fb: FormBuilder,
    private route: Router
  ) {
    
    this.userForm = this.fb.group({

      name: ['', Validators.required],
      password: ['', Validators.required],

    })

   }

  ngOnInit(): void {
  }

  ProceedLogin(forms: any) {
    if(this.userForm.valid) {
      this.userService.proceedLogin(this.userForm.value)
      .subscribe(res => {
        if(res != null) {
          this.responsedata = res;
          localStorage.setItem('token', this.responsedata.token);
          localStorage.setItem('username', this.responsedata.username);
          this.route.navigate([''])
        }
      })
    }
  }

}
