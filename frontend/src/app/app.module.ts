import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ListArticleComponent } from './list-article/list-article.component';
import { CreateArticleComponent } from './create-article/create-article.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { UpdateArticleComponent } from './update-article/update-article.component';
import { TokenInterceptorService } from './token-interceptor.service';
import { SingleArticleComponent } from './single-article/single-article.component';

@NgModule({
  declarations: [
    AppComponent,
    ListArticleComponent,
    CreateArticleComponent,
    SignupComponent,
    LoginComponent,
    UpdateArticleComponent,
    SingleArticleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
